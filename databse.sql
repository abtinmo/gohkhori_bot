CREATE TABLE users ( 
	user_id VARCHAR(50) PRIMARY KEY,
	is_bot BOOLEAN,
	first_name VARCHAR(500),
	last_name varchar(500),
	username varchar(500)
);

CREATE TABLE groups (
	gp_id VARCHAR(50) PRIMARY KEY,
	title varchar(500)
);

CREATE TABLE gohkhors(
	gohkhor_id VARCHAR(50),
	user_id varchar(50) REFERENCES users(user_id) ON DELETE CASCADE,
	chat_id  varchar(50) REFERENCES groups(gp_id)
);


CREATE OR REPLACE FUNCTION AddGroupToDB(inputGroupID varchar(25), inputTitle varchar(500))
	RETURNS INTEGER AS $$
	BEGIN
        	INSERT INTO groups(gp_id , title) VALUES ( inputGroupID  , inputTitle );
	RETURN 0;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION RemoveGoupFromDB(inputGroupID varchar(25))
	RETURNS INTEGER AS $$
	BEGIN
		DELETE FROM groups WHERE groups.gp_id = inputGroupID;
	RETURN 0;
END;
$$ LANGUAGE plpgsql;
	


