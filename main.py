import time
import telepot
from telepot.loop import MessageLoop
from config import TOKEN, DBconfig
import psycopg2


class Commands:

    def __init__(self, bot):
        self.bot = bot

    def getway(self, msg):
        print(msg)
        chat_type = msg["chat"]["type"]
        chat_id = msg["chat"]["id"]
        if chat_type == 'supergroup':
            chat_type = 'group'
        try:
            chat_message = msg["text"].strip('/')
            function_name = "{0}_{1}".format(chat_type, chat_message )
        except:
            if 'left_chat_participant' in msg:
                function_name = chat_type +  '_left_chat_participant'
            if 'new_chat_participant' in msg:
                function_name = chat_type + '_new_chat_participant'
        try:
            print(function_name)
            function = getattr(self, function_name)
            result = function( msg )
        except Exception as e:
            print(e)
            result = self.notFound()
        self.bot.sendMessage( chat_id, result)
        print("\n\n\n", chat_type )

    def private_help(self, msg):
        print("in function help")
        return "hi this is gohkhori_bot , this is help"

    def notFound(self):
        print("\n\n\n", "in function not found")
        return "Command not found , if you dont know how this bot works send \n /help"

    #TODO nedd to retun result
    def group_new_chat_participant(self, message):
        '''
        add group to databse 
        '''
        conn = psycopg2.connect(DBconfig)
        params = [ str(message["chat"]["id"]),message["chat"]["title"]  ]
        cur = conn.cursor()
        cur.callproc('AddGroupToDB', params)
        conn.commit()
        conn.close()

    def group_left_chat_participant(self, message):
        '''
        remove group from database
        '''
        conn = psycopg2.connect(DBconfig)
        params = [str(message["chat"]["id"]), ]
        cur = conn.cursor()
        cur.callproc('RemoveGoupFromDB', params)
        conn.commit()
        conn.close()
    


bot = telepot.Bot(TOKEN)
command_handler = Commands(bot)
MessageLoop(bot, command_handler.getway).run_as_thread(allowed_updates=[])
print('Listening ...')
MessageLoop

# Keep the program running.
while 1:
    time.sleep(1)
